/***************************************************************************//**
 * Martina Scholz
 * Martrikelnr: 547342
 * Compilerbau SS 2022
 * @file stack.c
 * @brief Implementation of an integer stack.
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

#define SIZE 1;


// Initialisiert einen neuen Stack und liefert den Rückgabewert 0,
// falls keine Fehler bei der Initialisierung aufgetreten sind und andernfalls einen Fehlercode.
int stackInit(IntStack *self) {
    self->top = -1;
    self->cap = SIZE;
    self->array = (int *) malloc(sizeof(int) * self->cap);
    if (self->array == NULL) {
        fputs("unable-to-allocate-memory error\n", stderr);
        exit(-1);
    }
    return 0;
}

// Gibt den Stack und alle assoziierten Strukturen frei.
void stackRelease(IntStack *self) {
    free(self->array);
}

// Legt den Wert von i auf den Stack.
void stackPush(IntStack *self, int i) {
    if (self->top == self->cap - 1) {
        self->cap *= 2;
        self->array = (int *) realloc(self->array, sizeof(int) * self->cap);
        if (self->array == NULL) {
            fputs("unable-to-allocate-memory error\n", stderr);
            exit(-1);
        }
    }
    self->array[++self->top] = i;
    return;
}

// Gibt das oberste Element des Stacks zurück.
int stackTop(const IntStack *self) {
    return self->array[self->top];
}

// Entfernt und liefert das oberste Element des Stacks.
int stackPop(IntStack *self) {
    if (self->top == -1) {
        fputs("empty-stack error\n", stderr);
        exit(-1);
    }
    return self->array[self->top--];
}

// Gibt zurück, ob ein Stack leer ist, d.h. kein Element enthält (Rückgabewert != 0, wenn leer; == 0, wenn nicht).
int stackIsEmpty(const IntStack *self) {
    return self->top == -1;
}

// Gibt den Inhalt des Stacks beginnend mit dem obersten Element auf der Standardausgabe aus.
void stackPrint(const IntStack *self) {
    for (int i = self->top; i >= 0; i--) {
        printf("%i\n", self->array[i]);
    }
}

